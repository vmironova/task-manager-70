package ru.t1consulting.vmironova.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1consulting.vmironova.tm.dto.model.TaskDTO;
import ru.t1consulting.vmironova.tm.dto.request.TaskListRequest;
import ru.t1consulting.vmironova.tm.enumerated.CustomSort;
import ru.t1consulting.vmironova.tm.event.ConsoleEvent;
import ru.t1consulting.vmironova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

@Component
public final class TaskListListener extends AbstractTaskListener {

    @NotNull
    public static final String DESCRIPTION = "Display task list.";

    @NotNull
    public static final String NAME = "task-list";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@taskListListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(CustomSort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final CustomSort sort = CustomSort.toSort(sortType);
        @NotNull final TaskListRequest request = new TaskListRequest(getToken());
        request.setSort(sort);
        @Nullable final List<TaskDTO> tasks = taskEndpoint.listTask(request).getTasks();
        if (tasks == null) return;
        @NotNull final int[] index = {1};
        tasks.forEach(m -> {
            System.out.println(index[0] + ". " + m);
            index[0]++;
        });
    }

}
