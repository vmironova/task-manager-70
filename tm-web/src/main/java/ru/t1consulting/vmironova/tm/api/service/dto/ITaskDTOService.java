package ru.t1consulting.vmironova.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.vmironova.tm.dto.model.TaskDTO;
import ru.t1consulting.vmironova.tm.enumerated.Status;

import java.util.List;

public interface ITaskDTOService {

    @NotNull
    TaskDTO add(@NotNull TaskDTO model) throws Exception;

    @NotNull
    TaskDTO addByUserId(@Nullable String userId, @NotNull TaskDTO model) throws Exception;

    void changeTaskStatusById(
            @Nullable String id,
            @Nullable Status status
    ) throws Exception;

    void changeTaskStatusByUserIdAndId(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    ) throws Exception;

    void clear() throws Exception;

    void clearByUserId(@Nullable String userId) throws Exception;

    int count() throws Exception;

    int countByUserId(@Nullable String userId) throws Exception;

    @NotNull
    TaskDTO create(@Nullable String userId, @Nullable String name) throws Exception;

    @NotNull
    TaskDTO create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    boolean existsById(@Nullable String id) throws Exception;

    boolean existsByUserIdAndId(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    List<TaskDTO> findAll() throws Exception;

    @Nullable
    List<TaskDTO> findAllByUserId(@Nullable String userId) throws Exception;

    @Nullable
    List<TaskDTO> findAllByProjectId(
            @Nullable String projectId
    ) throws Exception;

    @Nullable
    List<TaskDTO> findAllByUserIdAndProjectId(
            @Nullable String userId,
            @Nullable String projectId
    ) throws Exception;

    @Nullable
    TaskDTO findOneById(@Nullable String id) throws Exception;

    @Nullable
    TaskDTO findOneByUserIdAndId(@Nullable String userId, @Nullable String id) throws Exception;

    void remove(@Nullable TaskDTO model) throws Exception;

    void removeByUserId(@Nullable String userId, @Nullable TaskDTO model) throws Exception;

    void removeById(@Nullable String id) throws Exception;

    void removeByUserIdAndId(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    TaskDTO update(@Nullable TaskDTO model) throws Exception;

    @Nullable
    TaskDTO updateByUserId(@Nullable String userId, @Nullable TaskDTO model) throws Exception;

    @Nullable
    TaskDTO updateById(
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    @Nullable
    TaskDTO updateByUserIdAndId(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    void updateProjectIdById(
            @Nullable final String id,
            @Nullable final String projectId
    ) throws Exception;

    void updateProjectIdByUserIdAndId(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String projectId
    ) throws Exception;

}
