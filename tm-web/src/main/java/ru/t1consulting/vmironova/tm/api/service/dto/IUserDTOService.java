package ru.t1consulting.vmironova.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.vmironova.tm.dto.model.UserDTO;

import java.util.List;

public interface IUserDTOService {

    @NotNull
    UserDTO add(@NotNull UserDTO model) throws Exception;

    void clear() throws Exception;

    int count() throws Exception;

    boolean existsById(@Nullable String id) throws Exception;

    @Nullable
    List<UserDTO> findAll() throws Exception;

    @Nullable
    UserDTO findByLogin(@Nullable String login) throws Exception;

    @Nullable
    UserDTO findOneById(@Nullable String id) throws Exception;

    void remove(@Nullable UserDTO model) throws Exception;

    void removeById(@Nullable String id) throws Exception;

    @Nullable
    UserDTO update(@Nullable UserDTO model) throws Exception;

    @Nullable
    UserDTO updateById(
            @Nullable String id,
            @Nullable String login,
            @Nullable String password
    ) throws Exception;

}
