package ru.t1consulting.vmironova.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1consulting.vmironova.tm.api.service.dto.IUserDTOService;
import ru.t1consulting.vmironova.tm.dto.model.UserDTO;
import ru.t1consulting.vmironova.tm.exception.entity.UserNotFoundException;
import ru.t1consulting.vmironova.tm.exception.field.IdEmptyException;
import ru.t1consulting.vmironova.tm.exception.field.LoginEmptyException;
import ru.t1consulting.vmironova.tm.exception.field.PasswordEmptyException;
import ru.t1consulting.vmironova.tm.repository.UserDTORepository;

import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class UserDTOService implements IUserDTOService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Nullable
    @Autowired
    private UserDTORepository repository;

    @NotNull
    @Override
    @Transactional
    public UserDTO add(@NotNull final UserDTO model) throws Exception {
        return repository.save(model);
    }

    @Override
    @Transactional
    public void clear() throws Exception {
        repository.deleteAll();
    }

    @Override
    public boolean existsById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Nullable
    @Override
    public List<UserDTO> findAll() throws Exception {
        return repository.findAll();
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO result = repository.findByLogin(login);
        return result;
    }

    @Nullable
    @Override
    public UserDTO findOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Optional<UserDTO> result = repository.findById(id);
        return result.orElse(null);
    }

    @Override
    public int count() throws Exception {
        return (int) repository.count();
    }

    @Override
    @Transactional
    public void remove(@Nullable final UserDTO model) throws Exception {
        if (model == null) return;
        repository.delete(model);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.deleteById(id);
    }

    @Override
    @Transactional
    public UserDTO update(@Nullable final UserDTO model) throws Exception {
        if (model == null) return null;
        return repository.save(model);
    }

    @Override
    @Transactional
    public UserDTO updateById(
            @Nullable final String id,
            @Nullable final String login,
            @Nullable final String password
    ) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserDTO user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setLogin(login);
        user.setPasswordHash(passwordEncoder.encode(password));
        repository.save(user);
        return user;
    }

}
