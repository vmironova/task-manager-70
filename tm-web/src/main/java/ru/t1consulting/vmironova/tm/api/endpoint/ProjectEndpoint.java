package ru.t1consulting.vmironova.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.t1consulting.vmironova.tm.dto.model.ProjectDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@RequestMapping("/api/projects")
public interface ProjectEndpoint {

    @WebMethod
    @GetMapping("/findAll")
    List<ProjectDTO> findAll() throws Exception;

    @WebMethod
    @PostMapping("/add")
    ProjectDTO add(
            @WebParam(name = "project", partName = "project")
            @RequestBody ProjectDTO task
    ) throws Exception;

    @WebMethod
    @PostMapping("/save")
    ProjectDTO save(
            @WebParam(name = "project", partName = "project")
            @RequestBody ProjectDTO project
    ) throws Exception;

    @WebMethod
    @GetMapping("/findById/{id}")
    ProjectDTO findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) throws Exception;

    @WebMethod
    @GetMapping("/existsById/{id}")
    boolean existsById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) throws Exception;

    @WebMethod
    @GetMapping("/count")
    long count() throws Exception;

    @WebMethod
    @PostMapping("/deleteById/{id}")
    void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) throws Exception;

    @WebMethod
    @PostMapping("/delete")
    void delete(
            @WebParam(name = "project", partName = "project")
            @RequestBody ProjectDTO project
    ) throws Exception;

    @WebMethod
    @PostMapping("/deleteAll")
    void clear() throws Exception;

}
