<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../include/_header.jsp"/>

<h3>PROJECT EDIT</h3>

<form:form action="/project/edit/${project.id}" method="POST" modelAttribute="project">
    <form:input type="hidden" path="id"/>
    <p>
    <div>Name:</div>
    <div><form:input type="text" path="name"/></div>
    </p>
    <p>
    <div>Description:</div>
    <div><form:input type="text" path="description"/></div>
    </p>
    <p>
    <div>Status:</div>
    <div>
        <form:select path="status">
            <form:option value="${null}" label="-----"/>
            <form:options items="${statuses}" itemLabel="displayName"/>
        </form:select>
    </div>
    </p>
    <p>
    <div>Created:</div>
    <div><form:input type="date" path="created"/>
    </div>
    </p>
    <button type="submit" style="padding-top: 20px;">SAVE PROJECT</button>
</form:form>

<jsp:include page="../include/_footer.jsp"/>