package ru.t1consulting.vmironova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1consulting.vmironova.tm.model.User;

@Repository
@Scope("prototype")
public interface UserRepository extends AbstractRepository<User> {

    @NotNull
    User findByLogin(@NotNull final String login);

    @NotNull
    User findByEmail(@NotNull final String email);

    @Query("select case when count(c)> 0 then true else false end from User c where login = :login")
    boolean isLoginExists(@Param("login") String login);

    @Query("select case when count(c)> 0 then true else false end from User c where email = :email")
    boolean isEmailExists(@Param("email") String email);

}
