package ru.t1consulting.vmironova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1consulting.vmironova.tm.dto.model.TaskDTO;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface TaskDTORepository extends AbstractUserOwnedDTORepository<TaskDTO> {

    long countByUserId(@NotNull final String userId);

    void deleteByUserId(@NotNull final String userId);

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @Query("select case when count(c)> 0 then true else false end from TaskDTO c where userId = :userId and id = :id")
    boolean existByUserIdAndId(@Param("userId") String userId, @Param("id") String id);

    @NotNull
    List<TaskDTO> findByUserId(@NotNull final String userId);

    @NotNull
    List<TaskDTO> findByUserId(@NotNull final String userId, @NotNull final Sort sort);

    @NotNull
    Optional<TaskDTO> findByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @NotNull
    List<TaskDTO> findByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId);

}
