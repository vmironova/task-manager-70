package ru.t1consulting.vmironova.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.vmironova.tm.enumerated.Role;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_user")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class UserDTO extends AbstractModelDTO {

    private static final long serialVersionUID = 1;

    @Nullable
    @Column(nullable = false)
    private String login;

    @Nullable
    @Column(name = "password")
    private String passwordHash;

    @Column
    @NotNull
    private String email = "";

    @NotNull
    @Column(name = "first_name")
    private String firstName = "";

    @NotNull
    @Column(name = "last_name")
    private String lastName = "";

    @NotNull
    @Column(name = "middle_name")
    private String middleName = "";

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Role role = Role.USUAL;

    @Column
    @NotNull
    private Boolean locked = false;

}
