package ru.t1consulting.vmironova.tm.exception.entity;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1consulting.vmironova.tm.exception.AbstractException;

@NoArgsConstructor
public abstract class AbstractEntityNotFoundException extends AbstractException {

    public AbstractEntityNotFoundException(@NotNull String message) {
        super(message);
    }

    public AbstractEntityNotFoundException(
            @NotNull String message,
            @NotNull Throwable cause
    ) {
        super(message, cause);
    }

    public AbstractEntityNotFoundException(@NotNull Throwable cause) {
        super(cause);
    }

    public AbstractEntityNotFoundException(
            @NotNull String message,
            @NotNull Throwable cause,
            boolean enableSuppression,
            boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
